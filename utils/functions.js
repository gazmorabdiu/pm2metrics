const axios = require('axios');
const https = require('https');

const instance = axios.create({
  baseURL: 'https://580a-46-99-212-16.ngrok-free.app/',
  timeout: 10000,
  headers: { 'X-Custom-Header': 'foobar', 'Content-Type': 'application/json' },
  httpsAgent: new https.Agent({ keepAlive: true }),
});
const http = require('http');

const convertToPrometheusFormat = metrics => {
  let prometheusMetrics = '';

  for (const metric of metrics) {
    const { name, values } = metric;

    // Build metric line
    let metricLine = ``;

    // Add labels (if any)
    for (let { value, labels } of values) {
      metricLine += `${name}`;

      if (labels) {
        metricLine += `{${Object.entries(labels)
          .map(([key, value]) => `${key}="${value}"`)
          .join(',')}}`;
      }

      metricLine += ` ${value}`;
      metricLine += '\n';
    }
    // Add newline character
    metricLine += '\n';

    // Append metric line to the result
    prometheusMetrics += metricLine;
  }

  return prometheusMetrics;
};

const sendMetrics = async metricsData => {
  // const options = {
  //   hostname: 'https://8a50-46-99-212-16.ngrok-free.app ',
  //   port: 8000,
  //   path: '/metrics',
  //   method: 'POST',
  //   headers: {
  //     'Content-Type': 'application/json',
  //   },
  // };
  // try {
  //   axios.post('https://be89-46-99-212-16.ngrok-free.app/metrics', metricsData, () => {});
  // } catch (err) {
  //   console.log(err);
  // }
  // Create the HTTP request
  // const req = http.request(options, res => {
  //   console.log(`Metrics data sent, statusCode: ${res.statusCode}`);
  // });
  // // Handle any errors that might occur during the request
  // req.on('error', error => {
  //   console.error('Error sending metrics data:', error);
  // });
  // // Write the metrics data to the request body
  // req.write(JSON.stringify(metricsData));
  // // End the request
  // req.end();
};

module.exports = { convertToPrometheusFormat, sendMetrics };
